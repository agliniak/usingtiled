App = {}

function App:new()
	local newApp = {}
	setmetatable(newApp, self)
	self.__index = self

	return newApp
end

function App:load()
	
	self.testmap = self:loadMap()() -- loads then runs which returns the info in file into table

	mapWidthQuads = self.testmap.width
	mapHeightQuads = self.testmap.height
	print("Map:")
	print("Quad Width: "..mapWidthQuads)
	print("Quad Height: "..mapHeightQuads)
	

	tilesetImage = love.graphics.newImage(self.testmap.tilesets[1].image)
	tilesetWidth = self.testmap.tilesets[1].imagewidth
	tilesetHeight = self.testmap.tilesets[1].imageheight
	tileWidth = self.testmap.tilewidth
	tileHeight = self.testmap.tileheight
	
	print("Pixel Width: "..mapWidthQuads*tileWidth)
	print("Pixel Height: "..mapHeightQuads*tileHeight)
	print("-------------------------")

	quadWidth = tilesetWidth/tileWidth
	quadHeight = tilesetHeight/tileHeight
	quadTotal = quadWidth*quadHeight
	print("TileSet: "..self.testmap.tilesets[1].image)
	print("Width: "..tilesetWidth)
	print("Height: "..tilesetHeight)
	print("-------------------------")
	print("Quads:")
	print("Width:"..quadWidth)
	print("Height: "..quadHeight)
	print("Total: "..quadTotal)
	print("-------------------------")

	quads = {} 
	
	quadIndex = 1

	for rowIndex = 0, quadHeight-1 do

		for columnIndex = 0, quadWidth-1 do

			quads[quadIndex] = love.graphics.newQuad(columnIndex*tileWidth,rowIndex*tileHeight,
													 tileWidth,tileHeight,
													 tilesetWidth,tilesetHeight)
			
			quadIndex=quadIndex+1
			print(quadIndex)
		end
	end

	print("Quad {} Size: "..#quads)




end

function App:loadControls()
	
	return love.filesystem.load("controls.lua")
end

function App:loadMap()
	
	return love.filesystem.load("level1.lua")
end

function App:draw()

	love.graphics.setColor(255,255,255,255)

	dataIndex = 1
	for rowIndex=1, self.testmap.height do --start at top, go down
	
		for columnIndex =1, self.testmap.width do
		

			if self.testmap.layers[1].data[dataIndex] > 0 then --freaks out when no tile is drawn in map skips zeros in future 0 should be a either a warning no texture tile or transparent png. or in making maps dar all transparent of black first then other map elements
			love.graphics.draw(tilesetImage,quads[self.testmap.layers[1].data[dataIndex]],(columnIndex-1)*tileWidth,(rowIndex-1)*tileHeight)
			end
			dataIndex=dataIndex+1
		end
	end
end

function App:update(dt)

	
end
