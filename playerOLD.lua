Player = {}

function Player:new(x,y)
	local newPlayer = {}
	setmetatable(newPlayer, self)
	self.__index = self
	--make bas or in rect?
	--newPlayer.base = Base:new(x,y)?  is this needed if i have vectors? 
	-- yes i want this cuz non drawables will have positon but i dont need vector movment,
	--newPlayer.vector2d = Vector2d(newPlayer.base)?

	newPlayer.yGravity = 900--should be everywhere?
	newPlayer.yAcceleration = 200
	newPlayer.yVelLimit = 200
	newPlayer.xVelLimit = 200
	newPlayer.xAcceleration = 900
	newPlayer.xFriction = 300
	newPlayer.yFriction = 300
	newPlayer.airborne = true
	newPlayer.color = newColor(255,50,50,255)
	newPlayer.rect = Rectangle:new(x,y,10,15,255,50,50,255,"fill")
	newPlayer.floor = 535
	--!!!!! read about better idea then airborne,  APPLYGRAVITY,APPLYFRICTION

	return newPlayer

end

function Player:draw()

	self.rect:draw()

end

function Player:checkInput(dt,input)

	local tempinputHandler = input
	

	if tempinputHandler=="a"then

		self.rect.xVel = self.rect.xVel - (self.xAcceleration *dt)
end
	if tempinputHandler=="d"then
			
			self.rect.xVel = self.rect.xVel + (self.xAcceleration*dt)
end
	if tempinputHandler=="w"then
			
			self.rect.yVel = self.rect.yVel + (self.yAcceleration*dt)
		end
	if tempinputHandler=="s"then
			
			self.rect.yVel = self.rect.yVel - (self.yAcceleration*dt)	

	end

	
	 	if tempinputHandler==" "  then
	 		self.rect.yVel = self.rect.yVel + (self.yAcceleration)
	 		self.airborne = true
	 	end
	
	--tempinputHandler:ClearInput()
end

function Player:physics(dt)
	--set the speed and direction (velocity)
	self.rect.x = self.rect.x + (self.rect.xVel * dt)

	--apply friction
	--should i only apply friction only when button is released?
	--right now its always fighting xacceleration
	if	self.rect.xVel > 0 then
		self.rect.xVel = self.rect.xVel - (self.xFriction*dt)

		--stop the jitter, stop friction once its at zero
		if self.rect.xVel<=0 then
			self.rect.xVel=0
		end
		
		elseif	self.rect.xVel < 0 then
		self.rect.xVel = self.rect.xVel + (self.xFriction*dt)
			--stop the jitter, stop friction once its at zero
		if self.rect.xVel>=0 then
			self.rect.xVel=0
		end
	end

	--check if speed is trying to go to high
	if self.rect.xVel >= self.xVelLimit then
		self.rect.xVel = self.xVelLimit
	end
	if self.rect.xVel <= -self.xVelLimit then
		self.rect.xVel = -self.xVelLimit
	end

---y physics
self.rect.y = self.rect.y - (self.rect.yVel * dt)

if	self.rect.yVel > 0 then
		self.rect.yVel = self.rect.yVel - (self.yFriction*dt)

		--stop the jitter, stop friction once its at zero
		if self.rect.yVel<=0 then
			self.rect.yVel=0
		end
		
		elseif	self.rect.yVel < 0 then
		self.rect.yVel = self.rect.yVel + (self.yFriction*dt)
			--stop the jitter, stop friction once its at zero
		if self.rect.yVel>=0 then
			self.rect.yVel=0
		end
	end

if self.rect.yVel >= self.yVelLimit then
		self.rect.yVel = self.yVelLimit
	end
	if self.rect.yVel <= -self.yVelLimit then
		self.rect.yVel = -self.yVelLimit
	end

	--turning off gravity for player as i troubleshoot collision
	self.rect.yVel = self.rect.yVel - (self.yGravity * dt)
		
end
-- if self.rect.y >= self.floor then
-- 		self.rect.y = self.floor
-- 		self.airborne = false
-- 		--SENDEVENTTOQUEUE=PLAYERNOTAIRBORNE
-- 		eventHandler:queueAdd("PLAYER_AIRBORNE")
-- 		--remove not airboren from queue

-- 		-- print("airborne")
-- elseif self.rect.y < self.floor then
-- 	self.airborne = true
-- 	--SENDEVENTTOQUEUE=PLAYERAIRBORNE
-- 	eventHandler:queueAdd("PLAYER_NOT_AIRBORNE")
-- 	--remove airboren from queue
-- 	--print("not airborne")
-- end
--red box always moing, cuz its fighting friction and has very high accell
--print("xVel: ",self.rect.xVel,"        yVel",self.rect.yVel, "       Airborne?  ", self.airborne)
--end


function Player:update(dt,inputHandler)
	--self:checkEventQueue()
	
	self:checkInput(dt,inputHandler)
	self:physics(dt)
	--CHECKCOLLISON!!?!?!?
end

function Player:getXvel()
return self.rect.xVel
end

function Player:getYvel()
return self.rect.yVel
end

	







